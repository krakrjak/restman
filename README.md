[![pipeline status](https://gitlab.com/krakrjak/restman/badges/master/pipeline.svg)](https://gitlab.com/krakrjak/restman/-/commits/master) 

RESTMan is a program for sending web requests using a terminal user interface or TUI.

[[_TOC_]]

Right now, Windows is not supported directly due to limitations of the [Brick](https://hackage.haskell.org/package/brick) library used to generate the TUI. You can however, use Windows to build and execute RESTMan through WSL2 and a Debian distribution. See details below.

---

# Usage

    % stack exec restman -- --help
    RESTman, an HTTP(S) application

    Usage: restman [--help] [-m|--method METHOD] 
                       [--no-default-headers | --default-headers] 
                       [-h|--custom-header NAME_VALUE] 
                       [--payload PAYLOAD | --payload-file PATHNAME]
      A TUI application for interactively using the full range of HTTP(S) directly

    Available options:
      --help                   Show command-line help and abort normal operation.
      -m,--method METHOD       HTTP(S) Method (or "Verb") (default: "GET")
      --no-default-headers     Send ONLY the custom headers specified.
      --default-headers        Send custom headers after default headers. This is
                               the default behavior.
      -h,--custom-header NAME_VALUE
                               Add a custom header, argument is name:value (colon
                               separates name from value).
      --payload PAYLOAD        Specify (in UTF-8) payload directly on command-line
      --payload-file PATHNAME  Load payload from file on startup

    https://gitlab.com/krakrjak/restman

---
# Building

For those already familiar with Haskell and the Stack, you can get up and running quickly with the usual recipie of `stack setup` followed by `stack build`. As usual, `stack setup` only needs to be run once.

## Building on Debian (WSL2 Tested)

Install and configure your favorite shell, you are going to spend a lot of time in it. :)

Once you are comfortable install the package _haskell-stack_, _libtinfo-dev_, and _libz1g-dev_ using apt: `apt install haskell-stack libtinfo-dev libz1g-dev`

You will need to upgrade the Debian provided stack using `stack upgrade`. If needed it will prompt you for sudo access. Once your stack is up-to-date you need a Haskell compiler. Use `stack setup` to determine, fetch, and configure a compiler for your environment. You can check your installation using `stack ghci` to enter the GHC REPL.

Now you are ready to build RESTMan and all of its dependencies. `stack build`

## Building on M1 Mac

Install [ghcup](https://www.haskell.org/ghcup/), then `brew install llvm`. Then you will need the following setup in your environment:

```sh
export PATH="/opt/homebrew/opt/llvm@13/bin:$PATH"
export LDFLAGS="-L/opt/homebrew/opt/llvm@13/lib"
export CPPFLAGS="-I/opt/homebrew/opt/llvm@13/include"
```

Then you can successfully use `stack` to build the program. You will need some extra flags to make it all work. Here's what is reported to work currently: `stack --arch aarch64 --system-ghc build`.

---
# Running and Testing

Once you have _restman-exe_ built you can use stack to execute it with `stack exec restman-exe`. If you want to pass options to the program you can use the form `stack exec restman-exe -- OPTIONS FOR RESTMAN BINARY`. You can also install the binary to _~/.local_ with `stack install`. If you add _~/.local/bin_ to your __PATH__ you can run the program without the aid of the _stack_ executable.

# Changelog for restman
0.4.0
  ⬆️ Stackage LTS version 18.28
  👷 CI Build Image Pinned to Stackage Version

0.3.0
  ⬆️ Stackage LTS version 17.4

0.2.0
  ⬆️ Stackage LTS version 15.
  ✨ TUI and basic CLI (TUI launcher only).
  ✨ Most basic features landed here including HTTPS support.
  ✨ GET, POST, DELETE, etc. to a given URL.
  ✨ You can also specify custom headers and payloads.

## Unreleased changes
All of them

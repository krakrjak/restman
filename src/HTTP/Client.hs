module HTTP.Client
    ( -- * HTTP.Client helpers
      UseDefaultHeaders(..)
    , knownMethods
    , Method
      -- * wreq re-exports
    , customMethodWith
    , customPayloadMethodWith
    , defaults
    , headers
    , responseBody
      -- * http-types re-exports
    , Header
    ) where

-- http-types
import Network.HTTP.Types.Header (Header)

-- wreq
import Network.Wreq
  (customMethodWith, customPayloadMethodWith, defaults, headers, responseBody)

-- | Helper type for clarity of intention.
type Method = String

-- | Bool-ish (for now) with more descriptive names.
data UseDefaultHeaders
  = ReplaceDefaultHeaders
  | AppendCustomToDefaultHeaders
  deriving (Eq, Ord, Enum, Bounded, Show, Read)
{-|
Lifted from
https://en.wikipedia.org/w/index.php?title=Hypertext_Transfer_Protocol&oldid=954964381#Request_methods
-}
knownMethods :: [Method]
knownMethods =
  [ "GET"
  , "HEAD"
  , "POST"
  , "PUT"
  , "DELETE"
  , "TRACE"
  , "OPTIONS"
  , "CONNECT"
  , "PATCH"
  ]
